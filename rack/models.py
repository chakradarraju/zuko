from django.db import models

class Product(models.Model):
  pub_date = models.DateTimeField(auto_now=True)
  name = models.CharField(max_length=200)
  key = models.CharField(max_length=200)
  price = models.IntegerField()
  description = models.TextField()

  def __unicode__(self):
    return self.name
