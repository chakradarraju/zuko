from django.shortcuts import render

from rack.models import Product

def index(request):
  product_list = Product.objects.all().order_by('-pub_date')[:5]
  return render(request, 'rack/index.html', {'product_list': product_list})

def product(request, key):
  product_list = Product.objects.all().order_by('-pub_date')[:5]
  product = [a for a in product_list if a.key == key][0]
  product.image_url = "https://googledrive.com/host/0B7LN3K-7DTu4S05ZdU1Fc0FFXzA/products/" + product.key + ".jpg"
  return render(request, 'rack/product.html', {'product': product, 'product_list': product_list})
