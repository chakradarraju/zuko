from django.conf.urls import patterns, url
from django.conf.urls.static import static
from django.conf import settings
from rack import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^product/(?P<key>[a-zA-Z]+)/$', views.product, name='product'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
